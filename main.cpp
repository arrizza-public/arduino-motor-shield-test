#include "WProgram.h"
#include "AFMotor.h"

//===========================
// toggle the LED for visual effects
void toggleled()
  {
  static int state = 0;
  state = 1 - state;
  digitalWrite(13, state);
  }

//===========================
// some common parameters for run() function
enum
  {
  when = 10,
  steptime = 100,
  faster = 0,
  slower = 1,
  };

//===========================
class Motor
  {
  public:
    //-----------------------------
    Motor(int motorid)
        : mId(motorid), mMotor(mId)
      {
      init();
      }

    //-----------------------------
    // initialize the motor to zero speed
    void init()
      {
      mMotor.setSpeed(0);
      mMotor.run(RELEASE);
      }

    //-----------------------------
    // idle the motor for a while
    void idleFor(int mstimeout)
      {
      Serial.println("Idle...");
      toggleled();
      mMotor.run(RELEASE);
      delay(mstimeout);
      }

    //-----------------------------
    // run the motor, either forward or backward,
    // once it's running either increase/decrease the speed
    void run(uint8_t dir, uint8_t rate)
      {
      Serial.println();
      Serial.print("M");
      Serial.print(mId, DEC);
      Serial.print(": ");
      Serial.print(dir == FORWARD ? "Forward - " : "Backward - ");
      Serial.println(rate == slower ? "slower" : "faster");
      mMotor.run(dir);
      uint8_t start;
      uint8_t end;
      uint8_t inc;
      if (rate == faster)
        {
        start = 0;
        end = 255;
        inc = 1;
        }
      else
        {
        start = 255;
        end = 0;
        inc = -1;
        }

      for (uint8_t i = start; i != end; i += inc)
        {
        toggleled();
        if (i % when == 0)
          {
          Serial.print(i, DEC);
          Serial.print(" ");
          }
        mMotor.setSpeed(i);
        delay(steptime);
        }
      }

  private:
    int mId;
    AF_DCMotor mMotor;
  };

//===========================
Motor motor1(1);
Motor motor2(2);

//===========================
void setup()
  {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");

  // initialize the digital pin as an output.
  // Pin 13 has an LED connected on most Arduino boards:
  pinMode(13, OUTPUT);

  // initialize the motors
  motor1.init();
  motor2.init();
  }

//===========================
// run the motors forward and back
void loop()
  {
  toggleled();

  // ramp up, down and pause
  motor1.run(FORWARD, faster);
  motor1.run(FORWARD, slower);
  motor1.idleFor(100);

  // ramp up, down and pause
  motor2.run(FORWARD, faster);
  motor2.run(FORWARD, slower);
  motor2.idleFor(100);

  // ramp up, down and pause in reverse
  motor1.run(BACKWARD, faster);
  motor1.run(BACKWARD, slower);
  motor1.idleFor(100);

  // ramp up, down and pause in reverse
  motor2.run(BACKWARD, faster);
  motor2.run(BACKWARD, slower);
  motor2.idleFor(100);
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
